# OpenShift Minio Backup - Docker Image

## Summary

A :whale: Docker image used to backup database and yaml to s3 using restic

## Environnement variables

* `BACKUP_HOST` : url of s3 server (example : `https://s3.fr-par.scw.cloud`)
* `BUCKET` : s3 bucket name
* `GLOBAL_KINDS_BACKUP` : cluster kinds to backup (example : `namespace clusterrole clusterrolebinding storageclass`)
* `NAMESPACE_KINDS_BACKUP` : namespace kind to backup (example : `ingress deployment role rolebinding configmap secret svc ds networkpolicy statefulset cronjob bc sa is pvc dc route PrometheusRule`)

**Attention ! Only namespaces labelled with `backup=true` will be backed up !**

For a specific namespace, if you want to precise additionnal kinds to backup, add the annotations `additionnalKindsBackup: application appproject` 

## Installation

1. Create the service account:

```
oc create -f yaml/sa.yaml
```

2. Create the role :

```
oc create -f yaml/role.yaml
```

Add the permission:

```
oc policy add-cluster-role-to-user database-backup system:serviceaccount:`oc project -q`:database-backup
```

## To build the Docker image

- Build the image using docker
```bash
$ docker build -t openshift-s3-backup .
```

## Contributing
File issues in GitHub to report bugs or issue a pull request to contribute.

Dockerfile based on https://github.com/e-bits/openshift-client work