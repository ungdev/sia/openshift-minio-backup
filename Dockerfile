FROM frolvlad/alpine-glibc:latest

ARG OC_GZ_URL="https://github.com/okd-project/okd/releases/download/4.11.0-0.okd-2022-10-15-073651/openshift-client-linux-4.11.0-0.okd-2022-10-15-073651.tar.gz"

ENV OC_GZ_URL=${OC_GZ_URL}

# install the oc client tools
ADD ${OC_GZ_URL} /opt/oc/release.tar.gz

RUN apk add --no-cache bash gawk sed restic bash grep bc coreutils curl gzip ca-certificates && \
    tar -xzvf /opt/oc/release.tar.gz -C /opt/oc/ && \
    mv /opt/oc/oc /usr/bin/ && restic self-update && \
    rm -rf /opt/oc

WORKDIR /app
ADD . /app/  

CMD bash backup-databases.sh
